import java.util.Scanner;

public class Simples3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//DECLARAÇÃO DE VARIAVEIS 
		double celsius=0, fahrenheit=0;
		Scanner entrada = new Scanner(System.in);
		
		
		//ENTRADA DE DADOS
		System.out.println("DIGITE A TEMPERATURA:");
		celsius = entrada.nextDouble();
		
		
		//PROCESSAMENTO
		fahrenheit = (celsius * 9/5) +32;
		
		
		//SAIDA DE DADOS
		System.out.println("CELSIUS = "+celsius);
		System.out.println("FAHRENHEIT = "+fahrenheit);
	}

}