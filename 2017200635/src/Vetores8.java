import java.util.Scanner;

public class Vetores8 {
	
	public static void main(String[] args) {
		
		Scanner ler = new Scanner(System.in);
		int W[] = new int[10], V=0, ocorencias=0;
		
		for (int i = 0; i < W.length; i++) {
				System.out.println("Digite um numero: ");
				W[i] = ler.nextInt();
		}
		
		System.out.println("Digite um numero para procurar no vetor: ");
		V = ler.nextInt();
		
		for (int i = 0; i < W.length; i++) {
			if (W[i]==V) {
				ocorencias++;
			}
		}
		if (ocorencias==0) {
			System.out.println("O valor "+V+" n�o apareceu no vetor W");
		}else {
			System.out.println("Quantidade de vezes que o valor "+V+" foi encontrado no vetor �:"+ocorencias);
		
			for (int i = 0; i < W.length; i++) {
				if (W[i]==V) {
					System.out.println("Indice: "+i);
				}
			}
		}
	}
}
