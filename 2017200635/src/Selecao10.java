import java.util.Scanner;

public class Selecao10 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//DECLARAÇÃO DE VARIAVEIS
		Scanner entrada = new Scanner(System.in);
		int x=0,y=0,z=0;
		
		//ENTRADA DE DADOS
		System.out.println("DIGITE UM VALOR: ");
		x = entrada.nextInt();
		System.out.println("DIGITE UM VALOR: ");
		y = entrada.nextInt();
		System.out.println("DIGITE UM VALOR: ");
		z = entrada.nextInt();
	
		
		//PROCESSAMENTO/SAIDA DE DADOS
		if (x<=y) {
			if (y<=z) {
				System.out.println("NUMEROS ORDENADOS "+ x +" - "+ y +" - "+ z);
			} else {
				System.out.println("NUMEROS ORDENADOS "+ x +" - "+ z +" - "+ y);
			}
		} else {
			if (y<=z) {
				if (x<=z) {
					System.out.println("NUMEROS ORDENADOS "+ y +" - "+ x +" - "+ z);
				} else {
					System.out.println("NUMEROS ORDENADOS "+ y +" - "+ z +" - "+ x);
				}
			} else {
				if (x<=y) {
					System.out.println("NUMEROS ORDENADOS "+ z +" - "+ x +" - "+ y);
				} else {
					System.out.println("NUMEROS ORDENADOS "+ z +" - "+ y +" - "+ x);
				}
			}

		}
		
	}

}
