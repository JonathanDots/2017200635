import java.util.Scanner;

public class RepeticaoSelecao10 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//DECLARA��O DE VARIAVEIS
		Scanner entrada = new Scanner(System.in);
		double avaliacao1=0, avaliacao2=0, avaliacaoOptativa=0, media=0;
		int qtdAlunosAprovados=0, qtdAlunosReprovados=0, qtdAlunosExame=0;
		String opcao = new String("s");
		
		do {
			//ENTRADA DE DADOS
			do {
				System.out.println("DIGITE O VALOR DA PRIMEIRA AVALIA��O: ");
				avaliacao1 = entrada.nextDouble();
			} while (avaliacao1<0 || avaliacao1>10);
			do {
				System.out.println("DIGITE O VALOR DA SEGUNDA AVALIA��O: ");
				avaliacao2 = entrada.nextDouble();
			} while (avaliacao2<0||avaliacao2>10);
			do {
				System.out.println("DIGITE O VALOR DA AVALIA��O OPTATIVA (DIGITE -1 CASO N�O TENHA FEITO): ");
				avaliacaoOptativa = entrada.nextDouble();
			} while (avaliacaoOptativa<-1||avaliacaoOptativa>10);
			
			
			//PROCESSAMENTO
			if (avaliacaoOptativa ==-1) {
				media = (avaliacao1+avaliacao2)/2;
			} else {
				if (avaliacaoOptativa>=avaliacao1) {
					if(avaliacao1>=avaliacao2) {
						media = (avaliacaoOptativa+avaliacao1) /2;
					}else{
						media = (avaliacaoOptativa+avaliacao2)/2;	
					}
				}else {
					if(avaliacaoOptativa>=avaliacao2) {
						media = (avaliacaoOptativa+avaliacao1) /2;
					}else {
						media = (avaliacao1+avaliacao2)/2;
					}
				}		
			}

			//SAIDA DE DADOS
			if (media>=6) {
				System.out.println("ALUNO APROVADO: "+media);
				qtdAlunosAprovados++;
			} else {
				if (media<3) {
					System.out.println("ALUNO REPROVADO: "+media);
					qtdAlunosReprovados++;
				} else {
					System.out.println("ALUNO EM EXAME: "+media);
					qtdAlunosExame++;
				}
			}
			
			System.out.println("CALCULAR MEDIA DE OUTRO ALUNO [S]im [N]�o?");
			opcao = entrada.next();
		} while (opcao.equals("s"));
		System.out.println("ALUNOS APROVADOS: " +qtdAlunosAprovados);
		System.out.println("ALUNOS REPROVADOS: " +qtdAlunosReprovados);
		System.out.println("ALUNOS EM EXAME: " +qtdAlunosExame);
	}

}
