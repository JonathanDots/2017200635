import java.util.Scanner;

public class RepeticaoSelecao9 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//DECLARA��O DE VARIAVEIS
		Scanner entrada = new Scanner(System.in);
		double nota1=0, nota2=0, media=0;
		String opcao = new String("s");
		
		do {		
			//ENTRADA DE DADOS
			System.out.println("DIGITE A PRIMEIRA NOTA");
			nota1 = entrada.nextDouble();
			System.out.println("DIGITE A SEGUnDA NOTA");
			nota2 = entrada.nextDouble();
			
			//PROCESSAMENTO
			media=(nota1+nota2)/2;
			
			//SAIDA DE DADOS
			System.out.println("MEDIA: "+ media);
			System.out.println("CALCULAR A MEDIA DE OUTRO ALUNO [S]im [N]�o");
			opcao = entrada.next();
				
		} while (opcao.equals("s"));

	}

}
