import java.util.Scanner;

public class RepeticaoSelecao17 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//DECLARAÇÃO DE VARIAVEIS
		Scanner entrada = new Scanner(System.in);
		int fatorial=0, resultado=0;
		
		//ENTRADA DE DADOS
		System.out.println("DIGITE UM NUMERO PARA CALCULAR O FATORIAL: ");
		fatorial = entrada.nextInt();
		
		if (fatorial==0) {
			resultado++;
		}else {
			resultado++;
			for (int i = 2; i <= fatorial; i++) {
				resultado = resultado * i;
			}
		}
		
		System.out.println(resultado);
		
	}

}
