import java.util.Scanner;

public class Simples1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//DECLARAÇÃO DE VARIAVEIS 
		double raio=0, area=0;
		final double PI = 3.14159265359;
		Scanner entrada = new Scanner(System.in);
		
		
		//ENTRADA DE DADOS
		System.out.println("DIGITE O RAIO:");
		raio= entrada.nextDouble();
		
		
		//PROCESSAMENTO
		area = PI * Math.sqrt(raio);
		
		
		//SAIDA DE DADOS
		System.out.println("Area = "+area);
	}

}
