import java.util.Scanner;

public class Selecao19 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//DECLARA��O DE VARIAVEIS
		Scanner entrada = new Scanner(System.in);
		double numero1=0, numero2=0, numero3=0, maiorSoma=0;
		
		
		//ENTRADA DE DADOS
		System.out.println("DIGITE UM NUMERO: ");
		numero1 = entrada.nextDouble();
		System.out.println("DIGITE MAIS UM NUMERO: ");
		numero2 = entrada.nextDouble();
		System.out.println("DIGITE OUTRO NUMERO: ");
		numero3 = entrada.nextDouble();
		
		
		//PROCESSAMENTO
		if (numero1>=numero2) {
			if (numero2>=numero3) {
				maiorSoma = numero1+numero2;
			} else {
				maiorSoma = numero1+numero3;
			}
		} else {
			if (numero1>=numero3) {
				maiorSoma = numero1+numero2;
			} else {
				maiorSoma = numero2+numero3;
			}
		}
		
		
		//SAIDA DE DADOS
		System.out.println("MAIOR SOMA �: "+maiorSoma);
		
		
	}

}
