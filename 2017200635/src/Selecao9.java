import java.util.Scanner;

public class Selecao9 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		//DECLARA��O DE VARIAVEIS
		Scanner entrada = new Scanner(System.in);
		int qtdMaca=0;
		final double valorDuziaMaca = 0.25, valorMenosDuziaMaca = 0.30;
		double valorTotal;
		
		//ENTRADA DE DADOS
		System.out.println("DIGITE A QUANTIDADE DE MA��S: ");
		qtdMaca = entrada.nextInt();
		
		//PROCESSAMENTO/SAIDA
		if(qtdMaca>=12) {
			valorTotal = qtdMaca * valorDuziaMaca;
			System.out.println("VALOR DO PEDIDO: R$"+valorTotal);
		}else {
			valorTotal = qtdMaca * valorMenosDuziaMaca;
			System.out.println("VALOR DO PEDIDO: R$"+valorTotal);
		}
		
	}
	
}
