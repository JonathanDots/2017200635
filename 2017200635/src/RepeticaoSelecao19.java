
public class RepeticaoSelecao19 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//DECLARAÇÃO DE VARIAVEIS LOCAIS
		int soma=0;
		
		//PROCESSAMENTO
		for (int i = 100; i <= 200; i++) {
			if (i%2==0) {
				soma+=i;
			}
		}
		
		//SAIDA DE DADOS
		System.out.println("SOMA DOS NUMEROS PARES DE 100 a 200: "+soma);
	
	}

}
