import java.util.Scanner;

public class Simples2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//DECLARAÇÃO DE VARIAVEIS 
		double celsius=0, fahrenheit=0;
		Scanner entrada = new Scanner(System.in);
		
		
		//ENTRADA DE DADOS
		System.out.println("DIGITE A TEMPERATURA:");
		fahrenheit = entrada.nextDouble();
		
		
		//PROCESSAMENTO
		celsius = (fahrenheit-32) * 5 / 9;
		
		
		//SAIDA DE DADOS
		System.out.println("FAHRENHEIT = "+fahrenheit);
		System.out.println("CELSIUS = "+celsius);
	}

}
