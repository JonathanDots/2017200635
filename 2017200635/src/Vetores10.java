import java.util.Scanner;

public class Vetores10 {
	
	public static void main(String[] args) {
		
		Scanner ler = new Scanner(System.in);
		int nota1[] = new int[10], nota2[] = new int[10], medi[] = new int[10];
		int mediaTurma=0,qtdAlunoAcimaMedia=0;
		
		for (int i = 0; i < 10; i++) {
			System.out.println("digite a primeira nota do aluno "+i);
			nota1[i] = ler.nextInt();
			System.out.println("digite a segunda nota do aluno "+i);
			nota2[i] = ler.nextInt();
			medi[i]=(nota1[i]+nota2[i])/2;
			mediaTurma+=medi[i];
		}

		mediaTurma/=10;
		
		for (int i = 0; i < medi.length; i++) {
			if (mediaTurma<medi[i]) {
				qtdAlunoAcimaMedia++;
			}
		}
		System.out.println("Media da turma: "+mediaTurma);
		System.out.println("Quantidade de alunos acima da media: "+qtdAlunoAcimaMedia);
		
		
	}
}
