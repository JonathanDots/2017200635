import java.util.Scanner;

public class Vetores14 {

	public static void main(String[] args) {
		//DECLARAÇÃO DE VARIAVEIS
		Scanner ler = new Scanner(System.in);
		int U[] = new int[10],aux;
		
		//ENTRADA DE DADOS
		
		for (int i = 0; i < U.length; i++) {
			System.out.println("Digite um numero: ");
			U[i]= ler.nextInt();
		}
		
		for (int i = 4; i >= 0; i--) {
			aux=U[i];
			U[i]=U[(9-i)];
			U[(9-i)]=aux;
		}
		
		System.out.println("Vetor alterado");
		for (int i = 0; i < U.length; i++) {
			System.out.println(U[i]);
		}
	}

}
