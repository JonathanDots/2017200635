import java.util.Scanner;

public class Simples6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		//DECLARAÇÃO DE VARIAVEIS 
		double kmInico=0, kmFinal=0,kmPLitro=0, combustivelGasto=0, ganhoBruto=0, ganhoLiquido=0;
		final double valorCombustivel = 1.9;
		Scanner entrada = new Scanner(System.in);
		
		
		//ENTRADA DE DADOS
		System.out.println("DIGITE KM DO INICIO DO DIA");
		kmInico = entrada.nextDouble();
		System.out.println("DIGITE KM DO FINAL DO DIA");
		kmFinal = entrada.nextDouble();
		System.out.println("DIGITE QUANTOS LITROS DE COMBUSTIVEL FORAM GASTOS HOJE");
		combustivelGasto = entrada.nextDouble();
		System.out.println("DIGITE OS GANHOS DE HOJE");
		ganhoBruto = entrada.nextDouble();
		
		
		//PROCESSAMENTO
		kmPLitro = (kmFinal-kmInico)/combustivelGasto;
		ganhoLiquido = ganhoBruto-(combustivelGasto*valorCombustivel);


		//SAIDA DE DADOS
		System.out.println("KM/L FEITO HOJE: "+kmPLitro);
		System.out.println("GANHO LIQUIDO DO DIA: R$"+ganhoLiquido);
	}

}