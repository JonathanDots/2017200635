import java.util.Scanner;

public class Selecao16 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//DECLARA��O DE VARIAVEIS
		Scanner entrada = new Scanner(System.in);
		int numeroLados = 0;
		double medidaLado = 0;
		
		//ENTRADA DE DADOS
			System.out.println("DIGITE A QUANTIDADE DE LADOS");
			numeroLados = entrada.nextInt();
		System.out.println("QUAL A MEDIDA DO LADO (cm)");
		medidaLado = entrada.nextDouble();
		
		
		//PROCESAMENTO/SAIDA DE DADOS
		if (numeroLados<3) {
			System.out.println("N�O � UM POLIGONO");
		} else {
			if (numeroLados == 3) {
				System.out.println("TRIANGULO");
				System.out.println("PERIMETRO: "+medidaLado*3);
			} else {
				if (numeroLados == 4) {
					System.out.println("QUADRADO");
					System.out.println("AREA: "+medidaLado*medidaLado);
				} else {
					if (numeroLados == 5) {
						System.out.println("PENTAGONO");
					} else {
						System.out.println("POLIGONO N�O IDENTIFICADO");
					}
				}
			}			
			
		}
		
	
	}

}
