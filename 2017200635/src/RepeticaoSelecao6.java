import java.util.Scanner;

public class RepeticaoSelecao6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		//DECLARA��O DE VARIAVEIS
		Scanner entrada = new Scanner(System.in);
		double avaliacao1=0, avaliacao2=0, media=0;
		int opcao=0;
		
		
		do {	
			//ENTRADA DE DADOS
			do {
			System.out.println("DIGITE O VALOR DA PRIMEIRA AVALIA��O: ");
				avaliacao1 = entrada.nextDouble();
				if (avaliacao1 <0 || avaliacao1 >10) {
					System.out.println("NOTA INVALIDA");
				}
			} while (avaliacao1<0 || avaliacao1>10);
			do {
			System.out.println("DIGITE O VALOR DA SEGUNDA AVALIA��O: ");
				avaliacao2 = entrada.nextDouble();
				if (avaliacao2 <0 || avaliacao2 >10) {
					System.out.println("NOTA INVALIDA");
				}
			} while (avaliacao2<0 || avaliacao2>10);
		
		
			//PROCESSAMENTO
			media = (avaliacao1+avaliacao2)/2;
		
			//SAIDA DE DADOS
			System.out.println("MEDIA SEMESTRAL: "+media);
			System.out.println("NOVO CALCULO (1-SIM 2-N�O)");
			opcao = entrada.nextInt();
		} while (opcao==1);
	}

}
