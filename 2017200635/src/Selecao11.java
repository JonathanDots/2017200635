import java.util.Scanner;

public class Selecao11 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//DECLARAÇÃO DE VARIAVEIS
		Scanner entrada = new Scanner(System.in);
		short senha=0;
		
		//ENTRADA DE DADOS
		System.out.println("DIGITE A SENHA: ");
		senha = entrada.nextShort();
		
		//PROCESSAMENTO
		if (senha == 1234) {
			System.out.println("ACESSO PERMITIDO");
		} else {
			System.out.println("ACESSO NEGADO");
		}
		
	}

}
