import java.util.Scanner;

public class RepeticaoSelecao11 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//DECLARAÇÃO DE VARIAVEIS
		Scanner entrada = new Scanner(System.in);
		String senha ="teste", senhaDigitada="s";
		int contador=0;
		
		do {
			//ENTRADA DE DADOS
			System.out.println("DIGITE SUA SENHA:");
			senhaDigitada = entrada.next();
			contador++;
			if (senhaDigitada==senha) {
				System.out.println("ACESSO PERMITIDO");
				System.out.println("A SENHA FOI INFORMADA "+contador+"x");
			} else {
				System.out.println("ACESSO NEGADO");
			}
		} while (senhaDigitada!=senha);
		
	}

}
