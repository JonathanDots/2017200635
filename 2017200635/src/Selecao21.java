import java.util.Scanner;

public class Selecao21 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//DECLARAÇÃO DE VARIAVEIS
		Scanner entrada = new Scanner(System.in);
		double lado1=0, lado2=0, lado3=0;
		
		
		//ENTRADA DE DADOS
		System.out.println("DIGITE UM NUMERO: ");
		lado1 = entrada.nextDouble();
		System.out.println("DIGITE MAIS UM NUMERO: ");
		lado2 = entrada.nextDouble();
		System.out.println("DIGITE OUTRO NUMERO: ");
		lado3 = entrada.nextDouble();
	
		//PROCESSAMENTO
		if (lado1==lado2) {
			if (lado2==lado3) {
				System.out.println("TRIANGULO EQUILATERO");
			} else {
				System.out.println("TRIANGULO ISOSCELES");
			}
		} else {
			if (lado2==lado3) {
				System.out.println("TRIANGULO ISOSCELES");
			} else {
				if (lado1==lado3) {
					System.out.println("TRIANGULO ISOSCELES");
				} else {
				System.out.println("TRIANGULO ESCALENO");
				}
			}

		}
		
		
	
	
	}
}
