import java.util.Scanner;

public class Selecao6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		//DECLARAÇÃO DE VARIAVEIS
		Scanner entrada = new Scanner(System.in);
		double numero=0;
		
		//ENTRADA DE DADOS
		System.out.println("DIGITE UM NUMERO: ");
		numero = entrada.nextDouble();
		
		
		//PROCESSAMENTO/SAIDA DE DADOS
		if(numero==0) {
			System.out.println("ZERO");
		}else { 
			if (numero>0) {
			System.out.println("NUMERO POSITIVO");
			}else {
			System.out.println("NUMERO NEGATIVO");
			}
		}
	}

}