import java.util.Scanner;

public class Selecao17 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//DECLARA��O DE VARIAVEIS
		Scanner entrada = new Scanner(System.in);
		int valor1=0, valor2=0, operacao=0, resultado=0;
		
		
		//ENTRADA DE DADOS
		System.out.println("DIGITE UM VALOR INTEIRO");
		valor1 = entrada.nextInt();
		System.out.println("DIGITE OUTRO VALOR INTERO");
		valor2 = entrada.nextInt();
		do {
			System.out.println("QUAL OPERA��O? (1-ADI��O, 2-SUBTRA��O, 3-DIVIS�O, 4-MULTIPLICA��O)");
			operacao = entrada.nextInt();
		} while (operacao<1 | operacao>4);
		
		//PROCESSAMENTO
		switch (operacao) {
		case 1:
			resultado=valor1+valor2;
			break;
		case 2:
			resultado=valor1-valor2;
			break;
		case 3:
			resultado=valor1/valor2;
			break;
		case 4:
			resultado=valor1*valor2;
			break;
		}
		
		//SAIDA DE DADOS
		System.out.println("RESULTADO: "+resultado);
		
	}

}
