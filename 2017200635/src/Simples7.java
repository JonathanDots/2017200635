import java.util.Scanner;

public class Simples7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		//DECLARAÇÃO DE VARIAVEIS 
		double comprimentoPista=0, qtdVoltas=0, qtdReabastecimento=0, kmPLitros=0, qtdLitrosReabastecer=0;
		Scanner entrada = new Scanner(System.in);
		
		//ENTRADA DE DADOS
		System.out.println("COMPRIMENTO DA PISTA EM METROS: ");
		comprimentoPista = entrada.nextDouble();
		System.out.println("QUANTIDADE DE VOLTAS DO CIRCUITO: ");
		qtdVoltas = entrada.nextDouble();
		System.out.println("NUMERO DE REABASTECIMENTOS DESEJADOS: ");
		qtdReabastecimento = entrada.nextDouble();
		System.out.println("CONSUMO DO VEICULO (KM/L): ");
		kmPLitros = entrada.nextDouble();
		
		
		//PROCESSAMENTO
		qtdLitrosReabastecer = ((comprimentoPista * qtdVoltas)/qtdReabastecimento)/kmPLitros ;
		
		
		//SAIDA DE DADOS
		System.out.println("QUANTIDADE DE LITROS PARA ABASTECER O VEICULO: "+qtdLitrosReabastecer);
	}

}
