public class Vetores4 {

	public static void main(String[] args) {
		//DECLARAÇÃO DE VARIAVEIS
		int Vetor[] = new int[8],aux;
		Vetor[0]=5;
		Vetor[1]=1;
		Vetor[2]=4;
		Vetor[3]=2;
		Vetor[4]=7;
		Vetor[5]=8;
		Vetor[6]=3;
		Vetor[7]=6; 
		
		//ENTRADA DE DADOS
		for (int i = 7; i > 4; i--) {
			aux=Vetor[i];
			Vetor[i]=Vetor[((7-i)+1)];
			Vetor[((7-i)+1)]=aux;
		}
		
		Vetor[3]=Vetor[1];
		Vetor[Vetor[3]]=Vetor[Vetor[2]];
		for (int i = 0; i < 8; i++) {
			System.out.println("Vetor "+(i+1)+": "+Vetor[i] );
		}
	}

}
