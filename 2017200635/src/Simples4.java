import java.util.Scanner;


public class Simples4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//DECLARA��O DE VARIAVEIS 
		double potencia=0, potenciaTotal=0, largura=0, comprimento=0, qtdLampadas=0;
		Scanner entrada = new Scanner(System.in);
		
		
		//ENTRADA DE DADOS
		System.out.println("DIGITE A POTENCIA DA LAMPADA:");
		potencia = entrada.nextDouble();
		System.out.println("DIGITE A LARGURA DO COMODO:");
		largura = entrada.nextDouble();
		System.out.println("DIGITE O COMPRIMENTO DO COMODO:");
		comprimento = entrada.nextDouble();
		
		
		//PROCESSAMENTO
		potenciaTotal = potencia/(largura * comprimento);
		if (potenciaTotal<18) {
			do {
				qtdLampadas++;
				potenciaTotal = (potencia*qtdLampadas)/(largura*comprimento);
			} while (potenciaTotal<18);
			
			//SAIDA DE DADOS
			System.out.println("VOC� PRECISA DE "+qtdLampadas+" LAMPADAS DE "+potencia+" WATTS");
		} else {
			
			//SAIDA DE DADOS
			System.out.println("VOC� PRECISA DE 1 LAMPADA DE "+ potencia +" WATTS");
		}
		
	}

}