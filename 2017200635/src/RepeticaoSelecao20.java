import java.util.Scanner;

public class RepeticaoSelecao20 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//DECLARAÇÃO DE VARIAVEIS
		Scanner entrada = new Scanner(System.in);
		int qtdIdade=0, mediaIdade=0, idade=0;
		
		
		//ENTRADA DE DADOS
		do {
			if (idade!=0) {
				mediaIdade += idade;
				qtdIdade++;
			}
			
			System.out.println("DIGITE UMA IDADE OU DIGITE 0 PARA SAIR");
			idade = entrada.nextInt();
		} while (idade!=0);
		mediaIdade /= qtdIdade;
		
		//SAIDA DE DADOS
		
		System.out.println("QUANTIDADE DE IDADES LIDAS: "+qtdIdade);
		System.out.println("MEDIA DE IDADE: "+mediaIdade);
	}

}
