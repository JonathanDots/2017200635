import java.util.Scanner;

public class Selecao22 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//DECLARAÇÃO DE VARIAVEIS
		Scanner entrada = new Scanner(System.in);
		int homem1=0, homem2=0, mulher1=0, mulher2=0,soma=0,produto=0;
		
		
		//ENTRADA DE DADOS
		System.out.println("DIGITE A IDADE DO PRIMEIRO HOMEM");
		homem1 = entrada.nextInt();
		System.out.println("DIGITE A IDADE DO SEGUNDO HOMEM");
		homem2 = entrada.nextInt();
		System.out.println("DIGITE A IDADE DA PRIMEIRA MULHER");
		mulher1 = entrada.nextInt();
		System.out.println("DIGITE A IDADE DA SEGUNDA MULHER");
		mulher2 = entrada.nextInt();
		
		
		if (homem1>=homem2) {
			if (mulher1>=mulher2) {
				soma=homem1+mulher2;
				produto=homem2*mulher1;
			} else {
				soma=homem1+mulher1;
				produto=homem2*mulher2;
			}
		} else {
			if(mulher1>=mulher2) {
				soma=homem2+mulher2;
				produto=homem1*mulher1;
			}else {
				soma=homem2+mulher1;
				produto=homem1*mulher2;
			}
		}
		
		
		//SAIDA DE DADOS
		System.out.println("SOMA:"+soma);
		System.out.println("PRODUTO: "+produto);
	}
}		