import java.util.Scanner;

public class Selecao2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		//DECLARA��O DE VARIAVEIS
		Scanner entrada = new Scanner(System.in);
		double avaliacao1=0, avaliacao2=0, avaliacaoOptativa=0, media=0;
		
		//ENTRADA DE DADOS
		System.out.println("DIGITE O VALOR DA PRIMEIRA AVALIA��O: ");
		avaliacao1 = entrada.nextDouble();
		System.out.println("DIGITE O VALOR DA SEGUNDA AVALIA��O: ");
		avaliacao2 = entrada.nextDouble();
		System.out.println("DIGITE O VALOR DA AVALIA��O OPTATIVA (DIGITE -1 CASO N�O TENHA FEITO): ");
		avaliacaoOptativa = entrada.nextDouble();
		
		
		//PROCESSAMENTO
		if (avaliacaoOptativa ==-1) {
			media = (avaliacao1+avaliacao2)/2;
		} else {
			if (avaliacaoOptativa>=avaliacao1) {
				if(avaliacao1>=avaliacao2) {
					media = (avaliacaoOptativa+avaliacao1) /2;
				}else{
					media = (avaliacaoOptativa+avaliacao2)/2;	
				}
			}else {
				if(avaliacaoOptativa>=avaliacao2) {
					media = (avaliacaoOptativa+avaliacao1) /2;
				}else {
					media = (avaliacao1+avaliacao2)/2;
				}
			}		
		}
		
		
		//SAIDA DE DADOS
		if (media>=6) {
			System.out.println("ALUNO APROVADO: "+media);
		} else {
			if (media<3) {
				System.out.println("ALUNO REPROVADO: "+media);
			} else {
				System.out.println("ALUNO EM EXAME: "+media);
			}
		}
		
	}

}
