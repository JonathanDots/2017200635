public class Vetores3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//DECLARAÇÃO DE VARIAVEIS
		int[] Vetor = new int[10];
		
		//ENTRADA DE DADOS
		for (int i = 0; i < 10; i++) {
			if (i%2==0) {
				Vetor[i] = 1;
			} else {
				Vetor[i] = 0;
			}
		}
		
		for (int i = 0; i < 10; i++) {
			System.out.println("Vetor "+(i+1)+": "+Vetor[i] );
		}
		
	}

}
