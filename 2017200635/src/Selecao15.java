import java.util.Scanner;

public class Selecao15 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//DECLARAÇÃO DE VARIAVEIS
		Scanner entrada = new Scanner(System.in);
		int numeroLados = 0;
		double medidaLado = 0;
		
		//ENTRADA DE DADOS
		do {
			System.out.println("DIGITE A QUANTIDADE DE LADOS");
			numeroLados = entrada.nextInt();
		} while (numeroLados<3 || numeroLados>5);
		System.out.println("QUAL A MEDIDA DO LADO (cm)");
		medidaLado = entrada.nextDouble();
		
		
		//PROCESAMENTO/SAIDA DE DADOS
		if (numeroLados == 3) {
			System.out.println("TRIANGULO");
			System.out.println("PERIMETRO: "+medidaLado*3);
		} else {
			if (numeroLados == 4) {
				System.out.println("QUADRADO");
				System.out.println("AREA: "+medidaLado*medidaLado);
			} else {
				System.out.println("PENTAGONO");
			}
		}
	
	}

}
