import java.util.Scanner;

public class Selecao23 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//DECLARA��O DE VARIAVEIS
		Scanner entrada = new Scanner(System.in);
		double angulo1=0, angulo2=0, angulo3=0;
		
		
		//ENTRADA DE DADOS
		System.out.println("DIGITE UM NUMERO: ");
		angulo1 = entrada.nextDouble();
		System.out.println("DIGITE MAIS UM NUMERO: ");
		angulo2 = entrada.nextDouble();
		System.out.println("DIGITE OUTRO NUMERO: ");
		angulo3 = entrada.nextDouble();
	
		//PROCESSAMENTO
		if (angulo1<90) {
			if (angulo2<90) {
				if (angulo3<90) {
					System.out.println("TRI�NGULO ACUT�NGULO");
				} else {
					if (angulo3>90) {
						System.out.println("TRI�NGULO OBTUS�NGULO");
					}else {
						System.out.println("TRI�NGULO RET�NGULO");
					}
				}
			} else {
				if (angulo2>90) {
					System.out.println("TRI�NGULO OBTUS�NGULO");
				} else {
					System.out.println("TRI�NGULO RET�NGULO");
				}
			}
		} else {
			if (angulo1>90) {
				System.out.println("TRI�NGULO OBTUS�NGULO");
			} else {
				System.out.println("TRI�NGULO RET�NGULO");
			}
		}
	}
}
