import java.util.Scanner;

public class RepeticaoSelecao21 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//DECLARA��O DE VARIAVEIS
		Scanner entrada = new Scanner(System.in);
		int[] altura=new int[10],sexo=new int[10];
		int maiorAltura=0, menorAltura=0,qtdHomem=0,qtdMulher=0,mediaAlturaMulher=0;
		
		//ENTRADA DE DADOS
		for (int cont = 0; cont < 10; cont++) {


			System.out.println("DIGITE A "+(cont+1)+" ALTURA");
			altura[cont] = entrada.nextInt();
			if (cont==0) {
				maiorAltura=altura[cont];
				menorAltura=altura[cont];
			} else {
				if (altura[cont]>maiorAltura) {
					maiorAltura=altura[cont];
				}
				if (menorAltura<altura[cont]) {
					menorAltura=altura[cont];
				}
			}
			
			do {
				System.out.println("DIGITE O "+(cont+1)+" SEXO (1-MASCULINO/2-FEMININO)");
				sexo[cont] = entrada.nextInt();
				if (sexo[cont]==1) {
					qtdHomem++;
				}else {
					qtdMulher++;
					mediaAlturaMulher+=altura[cont];
				}
			} while (sexo[cont]<1 || sexo[cont]>2);
		}
		
		
		//SAIDA DE DADOS
		
		System.out.println("MAIOR ALTURA: "+maiorAltura);
		System.out.println("MENOR ALTURA: "+menorAltura);
		if (qtdMulher<1) {
			System.out.println("N�O HOUVE MULHERES");
		}else {
			System.out.println("A MEDIA DE ALTURA DAS MULHERES �: "+(mediaAlturaMulher/qtdMulher)+"cm");	
		}
		System.out.println("QUANTIDADE DE HOMENS: "+qtdHomem);
	}

}
